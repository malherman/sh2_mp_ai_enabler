use std::{
    env,
    error::Error,
    io::{stdout, Write},
};

#[cfg(target_os = "linux")]
mod linux;
#[cfg(target_os = "windows")]
mod windows;

const PROCESS_NAME: &str = "Stronghold2.exe";
const POINTER_OFFSET: u64 = 0x2A0F69;
const NEW_OPCODE: usize = 0x90909090909090;
const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() -> Result<(), Box<dyn Error>> {
    #[cfg(target_os = "linux")]
    use linux::*;
    #[cfg(target_os = "windows")]
    use windows::*;

    println!("Stronghold 2 Multiplayer AI Enabler {VERSION}\n");
    print!("Starting to look for Stronghold 2 process...");

    stdout().flush()?;
    let shpid = find_stronghold_pid(PROCESS_NAME).map_err(|err| {
        println!();
        err
    })?;
    println!(" found PID: {shpid}\n");
    println!("Finding address for instruction that disables AI.");

    stdout().flush()?;
    let aval = get_base_address(shpid, PROCESS_NAME)? + POINTER_OFFSET;
    println!("Found memory address: {aval:x}");
    println!("Overwriting instruction that disables AI.");

    stdout().flush()?;
    write_into_memory(shpid, aval, NEW_OPCODE)?;
    println!("Successful, AI will now remain enabled.");
    Ok(())
}
