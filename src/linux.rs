use std::{
    error::Error,
    fs::{File, OpenOptions},
    io::{BufRead, BufReader, Read},
    os::unix::fs::FileExt,
    path::PathBuf,
};

pub fn find_stronghold_pid(pname: &str) -> Result<u32, Box<dyn Error>> {
    const PROC_DIR: &str = "/proc";
    const COMM_DIR: &str = "comm";

    let mut buffer = vec![0; pname.len()];
    let proc_path = PathBuf::from(PROC_DIR);
    for (mut file, path) in PathBuf::from(PROC_DIR)
        .read_dir()?
        .filter_map(|en| en.ok())
        .map(|en| en.file_name())
        .map(|fname| proc_path.join(fname).join(COMM_DIR))
        .filter_map(|fpath| File::open(&fpath).ok().map(|f| (f, fpath)))
    {
        match file.read(&mut buffer) {
            Ok(_) => (),
            Err(_) => continue,
        }

        if buffer.as_slice() == pname.as_bytes() {
            return Ok(path
                .iter()
                .nth(2)
                .unwrap()
                .to_str()
                .unwrap()
                .trim()
                .parse()?);
        }
    }
    Err("error while attempting to find Stronghold 2 pid".into())
}

pub fn get_base_address(shpid: u32, proc_name: &str) -> Result<u64, Box<dyn Error>> {
    let path = PathBuf::from_iter(["/proc", &shpid.to_string(), "maps"]);
    let buf_reader = BufReader::new(File::open(path)?);
    for line in buf_reader.lines() {
        let line = line?;
        if line.contains(proc_name) {
            return Ok(u64::from_str_radix(
                line.split('-').next().unwrap().trim(),
                16,
            )?);
        }
    }
    Err("could not find base module address".into())
}

pub fn write_into_memory(shpid: u32, aval: u64, new_opcode: usize) -> Result<(), Box<dyn Error>> {
    const V_BYTES: usize = 7;

    OpenOptions::new()
        .write(true)
        .open(PathBuf::from_iter(["/proc", &shpid.to_string(), "mem"]))
        .and_then(|f| {
            let new_opcode = &new_opcode.to_ne_bytes()[0..V_BYTES];
            f.write_all_at(new_opcode, aval)?;
            f.write_all_at(new_opcode, aval + V_BYTES as u64)
        })
        .map_err(Into::into)
}
