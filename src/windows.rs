use std::{
    error::Error,
    mem::size_of,
    ptr::{addr_of, addr_of_mut},
};
use windows::{
    self,
    Win32::{
        Foundation::{CloseHandle, INVALID_HANDLE_VALUE},
        System::{
            Diagnostics::{
                Debug::WriteProcessMemory,
                ToolHelp::{
                    CreateToolhelp32Snapshot, Module32First, Module32Next, Process32First,
                    Process32Next, MODULEENTRY32, PROCESSENTRY32, TH32CS_SNAPMODULE,
                    TH32CS_SNAPMODULE32, TH32CS_SNAPPROCESS,
                },
            },
            Threading::{
                OpenProcess, PROCESS_QUERY_INFORMATION, PROCESS_VM_OPERATION, PROCESS_VM_READ,
                PROCESS_VM_WRITE,
            },
        },
    },
};

pub fn find_stronghold_pid(pname: &str) -> Result<u32, Box<dyn Error>> {
    let errmsg = "error while attempting to obtain PID";
    let mut proc = PROCESSENTRY32 {
        dwSize: size_of::<PROCESSENTRY32>() as u32,
        ..Default::default()
    };

    let hsnap = match unsafe { CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0) } {
        Ok(h) if h == INVALID_HANDLE_VALUE => return Err(errmsg.into()),
        Ok(h) => h,
        Err(_) => return Err(errmsg.into()),
    };
    if unsafe { Process32First(hsnap, addr_of_mut!(proc)) }.is_err() {
        unsafe {
            let _ = CloseHandle(hsnap);
        };
        return Err(errmsg.into());
    }
    while unsafe { Process32Next(hsnap, addr_of_mut!(proc)) }.is_ok() {
        if &proc.szExeFile[..pname.len()] == pname.as_bytes() {
            unsafe {
                let _ = CloseHandle(hsnap);
            };
            return Ok(proc.th32ProcessID);
        }
    }

    unsafe {
        let _ = CloseHandle(hsnap);
    };
    Err(errmsg.into())
}

pub fn get_base_address(shpid: u32, proc_name: &str) -> Result<u64, Box<dyn Error>> {
    let errmsg = "error while attempting to get base module address";
    let mut modentry = MODULEENTRY32 {
        dwSize: size_of::<MODULEENTRY32>() as u32,
        ..MODULEENTRY32::default()
    };

    let hsnap =
        match unsafe { CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, shpid) } {
            Ok(h) if h == INVALID_HANDLE_VALUE => {
                return Err(errmsg.into());
            }
            Ok(h) => h,
            Err(_) => {
                return Err(errmsg.into());
            }
        };

    if unsafe { Module32First(hsnap, addr_of_mut!(modentry)) }.is_ok() {
        while unsafe { Module32Next(hsnap, addr_of_mut!(modentry) as *mut _) }.is_ok() {
            if &modentry.szModule[..proc_name.len()] == proc_name.as_bytes() {
                unsafe {
                    let _ = CloseHandle(hsnap);
                };
                return Ok(modentry.modBaseAddr as u64);
            }
        }
    }
    unsafe {
        let _ = CloseHandle(hsnap);
    };
    Err(errmsg.into())
}

pub fn write_into_memory(shpid: u32, aval: u64, new_opcode: usize) -> Result<(), Box<dyn Error>> {
    const V_BYTES: usize = 7;

    let access =
        PROCESS_VM_READ | PROCESS_QUERY_INFORMATION | PROCESS_VM_WRITE | PROCESS_VM_OPERATION;
    let mut nwritten = 0usize;
    let proc = match unsafe { OpenProcess(access, true, shpid) } {
        Ok(proc) => proc,
        Err(_) => return Err(format!("error while opening handle to PID: {}", shpid).into()),
    };

    if unsafe {
        WriteProcessMemory(
            proc,
            aval as *const _,
            addr_of!(new_opcode) as *const _,
            V_BYTES,
            Some(addr_of_mut!(nwritten)),
        )
    }
    .is_err()
    {
        let _ = unsafe { CloseHandle(proc) };
        return Err(format!("error while opening handle to PID: {}", shpid).into());
    }
    if unsafe {
        WriteProcessMemory(
            proc,
            (aval + V_BYTES as u64) as *const _,
            addr_of!(new_opcode) as *const _,
            V_BYTES,
            Some(addr_of_mut!(nwritten)),
        )
    }
    .is_err()
    {
        let _ = unsafe { CloseHandle(proc) };
        return Err(format!("error while opening handle to PID: {}", shpid).into());
    }

    let _ = unsafe { CloseHandle(proc) };
    Ok(())
}
