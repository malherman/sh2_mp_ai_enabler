# NAME

sh2\_mp\_ai\_enabler - Enables Multiplayer AI in Stronghold 2 Steam Edition, now you can play cooperatively with your friends against the AI.

## Description

***This is a fork of https://gitlab.com/Daerandin/sh2_mp_ai_enabler, but realized in Rust and with simplified installation method***

**Only the game host should use this tool. Other players joining the game should not be running this tool.**

Launch this program first, then just start up Stronghold 2 from Steam. You can now host a multiplayer game and add AI opponents.

The actual AI in the game is not modified in any way, this program just makes it possible to add the existing single player AI to multiplayer.

### How it works

This program has been updated to work a little different from previous versions.

When you host a multiplayer game, Stronghold 2 overwrites a variable in memory to 0. This variable controls if you can see the button to add AI opponents. When you leave the multiplayer lobby, the game changes this variable again to be 1. Previous versions of this program would change this variable to 1 every second. This ensured that any changes made by the game were quickly overwritten, and you could add AI.

The current version of this program now works by overwriting this instruction in memory. The whole instruction, which is 7 bytes long, is overwritten with 7 NOP codes (0x90). In addition, this program also does the same for the button that lets you add randomized AI opponents. These changes ensure that the game will no longer change the variable to 0. This means that the option to add AI will always be present, both in single player Kingmaker as well as multiplayer.

No game files are modified by this program, so there are no permanent changes.

## Building

### with Cargo

For compilation you need installed Rust toolchain: https://www.rust-lang.org/tools/install

In the root directory:
```bash
$ cargo build --release && cp target/release/sh2_mp_ai_enabler .
```

### with Makefile (not recommended)

There is a Makefile available for building on both Linux and Windows (MSYS2 is needed).

In the root directory:
```bash
$ make
```

## Usage

After Stronghold 2 game started, run compiled before  `sh2_mp_ai_enabler`

## Authors

Daniel Jenssen <daerandin@gmail.com> (https://gitlab.com/Daerandin/sh2_mp_ai_enabler)<br>
German Maltsev <maltsev.german@gmail.com>
