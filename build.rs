use std::{env, error::Error};

fn main() -> Result<(), Box<dyn Error>> {
    if !["windows", "linux"].contains(&env::var("CARGO_CFG_TARGET_OS")?.as_str()) {
        return Err("unsupported platform".into());
    }

    Ok(())
}
